terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.21.0"
    }
  }
}

variable "do_token" {
  type = string
  description = "Personal Access Token on DigitalOcean."
  sensitive = true
  nullable = false
}

variable "public_key" {
  type = string
  description = "Public Key used for Digital Ocean Droplet."
  sensitive = true
  nullable = false
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "my_ssh_key" {
  name       = "Terraform DigitalOcean Example SSH Pub Key"
  public_key = file(var.public_key)
}

resource "digitalocean_droplet" "web_droplet_smallest" {
  image  = "ubuntu-22-04-x64"
  name   = "code-warriors-202207030030"
  region = "fra1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.my_ssh_key.fingerprint]
}

resource "digitalocean_reserved_ip" "web_droplet_smallest_ip" {
  droplet_id = digitalocean_droplet.web_droplet_smallest.id
  region     = digitalocean_droplet.web_droplet_smallest.region
}

resource "digitalocean_database_cluster" "mysql-db-cluster" {
  name       = "mysql-db-cluster"
  engine     = "mysql"
  version    = "8"
  size       = "db-s-1vcpu-1gb"
  region     = "fra1"
  node_count = 1
}

resource "digitalocean_database_firewall" "mysql_db_fw" {
  cluster_id = digitalocean_database_cluster.mysql-db-cluster.id

  rule {
    type  = "droplet"
    value = digitalocean_droplet.web_droplet_smallest.id
  }
}

resource "digitalocean_database_user" "swarmfly_db_user" {
  cluster_id = digitalocean_database_cluster.mysql-db-cluster.id
  name       = "swarmfly_db_user"
}

resource "digitalocean_database_db" "swarmfly_db" {
  cluster_id = digitalocean_database_cluster.mysql-db-cluster.id
  name       = "swarmfly_db"
}
